var grid = [
    [5, 3, 0, 0, 7, 0, 0, 0, 0],
    [6, 0, 0, 1, 9, 5, 0, 0, 0],
    [0, 9, 8, 0, 0, 0, 0, 6, 0],
    [8, 0, 0, 0, 6, 0, 0, 0, 3],
    [4, 0, 0, 8, 0, 3, 0, 0, 1],
    [7, 0, 0, 0, 2, 0, 0, 0, 6],
    [0, 6, 0, 0, 0, 0, 2, 8, 0],
    [0, 0, 0, 4, 1, 9, 0, 0, 5],
    [0, 0, 0, 0, 8, 0, 0, 7, 9],
];


let sudoku = function (board) {
    const size = 9;
    const boxSize = 3;

    const findEmptyPosition = (board) => {
        for (let row = 0; row < size; row++) {
            for (let col = 0; col < size; col++) {
                if (board[row][col] === 0) {
                    return [row, col];
                }
            }
        }
        return null;
    }

    const checkRows = (board, num, row, col) => {
        for (let i = 0; i < size; i++) {
            if (board[i][col] === num && i !== row) {
                return false;
            }
        }
        return true;
    };

    const checkCols = (board, num, row, col) => {
        for (let i = 0; i < size; i++) {
            if (board[row][i] === num && i !== col) {
                return false;
            }
        }
        return true;
    }

    const checkBox = (board, num, row, col) => {
        const boxRow = Math.floor(row / boxSize) * boxSize;
        const boxCol = Math.floor(col / boxSize) * boxSize;

        for (let i = boxRow; i < boxRow + boxSize; i++) {
            for (let j = boxCol; j < boxCol + boxSize; j++) {
                if (board[i][j] === num && i !== row && j !== col) {
                    return false;
                }
            }
        }
        return true;
    }

    const validate = (num, position, board) => {
        const [row, col] = position;

        //Check rows
        if (!checkRows(board, num, row, col)) {
            return;
        }

        //Check cols
        if (!checkCols(board, num, row, col)) {
            return;
        }

        //Check box
        if (!checkBox(board, num, row, col)) {
            return;
        }

        return true;
    }

    const solve = () => {
        const currPos = findEmptyPosition(board);

        if (currPos === null) {
            return true;
        }
        for (let i = 1; i < size + 1; i++) {
            const currNum = i;
            const isValid = validate(currNum, currPos, board);
            if (isValid) {
                const [x, y] = currPos;
                board[x][y] = currNum;

                if (solve()) {
                    return true;
                }

                board[x][y] = 0;
            }
        }

        return false;
    }

    solve();
    return board;
};


console.log(sudoku(grid));
